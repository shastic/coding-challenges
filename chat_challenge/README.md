# Pair-Programming Exercise

## Context

At Shastic we expect engineers to be able to understand business requirements, be able to learn how applications work in a short period of time and come up with elegant solutions for challenges.

This file contains a short explanation of how the application you're going to be working on during the exercise functions. Please read and study how to set it up properly in your local machine and how the current specification works.

At the time of your pair programming session, we will provide you with a set of hypothetical challenges that users of this app are facing.

You're expected to get an understanding of their challenges and come up with a solution.

For this exercise, you will be working on a software as a service platform that needs improvement for customer satisfaction. The application enables organizations to communicate with customers through a chat messaging application. The platform allows an organization to have multiple loan officers that use the application at the same time to communicate with customers.

## Current Specification

The current structure of the application is as follows:

```
*Organization*
  has_many :messages
  has_many :conversations
  has_many :loan_officers

*Loan Officer*
  belongs_to :organization
  has_many :messages, as: :author

*Person*
  has_many :conversations

*Conversation*
  belongs_to :organization
  belongs_to :person
  has_many :messages

*Message*
  belongs_to :organization
  belongs_to :conversation
  belongs_to :author, polymorphic: true
```

```
ConversationsController#index
  when no officer is logged in
    redirects to authentication path
  when logged in officer
    when the organization does not have any conversations
      displays conversations table
    when another organization has conversations and the current organization does not
      does not display conversations belonging another organization
    when the organization has conversations
      displays conversations
```

```
Conversation
  without valid person
    is expected to be invalid
  #last_message_from_officer
    when not having any messages
      is expected to be nil
    when owning messages from officer
      returns an array of messages
  with valid organization and user
    is expected to be valid
  #last_contact_name
    when no one contacted with conversation
      returns the last agent that contacted with the conversation
    when a loan_officer contacted with conversation
      returns the last agent that contacted with the conversation
    when multiple loan_officers contacted with conversation
      returns the last agent that contacted with the conversation
  #last_contact
    when no one contacted with conversation
      returns the last agent that contacted with the conversation
    when a loan_officer contacted with conversation
      returns the last agent that contacted with the conversation
    when multiple loan_officers contacted with conversation
      returns the last agent that contacted with the conversation
  #last_message_body
    when owning messages
      returns an array of messages
    when not having any messages
      is expected to be nil
  #messages
    when owning messages
      returns an array of messages
    when not having any messages
      is expected to be empty
  #last_message
    when not having any messages
      is expected to be nil
    when owning messages
      returns an array of messages
  without valid organization
    is expected to be invalid
```

```
Message
  without valid attributes
    is expected to include "must exist"
    is expected to include "must exist"
    is expected to include "can't be blank"
    is expected to be invalid
  with valid attributes
    is expected to be valid
  .from_officer
    when messages from people exist
      does not return messages from other person
      is expected to be empty
    when messages from loan officers exist
      returns messages created by the loan officer
    when no messages exist
      is expected to be empty
```

```
LoanOfficer
  #messages
    when loan_officer is the author of a message
      returns that message
    when loan_officer is not the author of any messages
      is expected to be empty
  validations
    with valid attributes
      is expected to be valid
    without valid attributes
      is expected to be invalid
```

```
Organization
  validity
    with valid attributes
      is expected to be valid
    with invalid attributes
      is expected to be invalid
  #messages
    when owning messages
      returns an array of messages
    when not having any messages
      is expected to be empty
  #loan_officers
    when not having any officers
      is expected to be empty
    when owning loan_officers
      returns an array of loan_officers
```

```
Person
  with valid attributes
    is expected to be valid
  with invalid attributes
    is expected to be invalid
```

## How to set up

System requirements:
* Docker

Note: Make sure to execute the following commands within the `chat_challenge` directory provided.

1. Build the docker container
```
docker-compose build
```

2. Start the docker container
```
docker-compose up -d
```

3. Access the docker container
```
docker-compose exec web bash
```

At this point you should be inside the dockerized Ruby environment inside the container and you can start the rails app, run tests, etc.



