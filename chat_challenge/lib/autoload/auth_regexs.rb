# frozen_string_literal: true

module AuthRegexs
  def self.included(base)
    base.extend(ClassMethods)
  end

  module ClassMethods
    protected

    def mobile_regex
      /\A[+]?1? ?\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})\z/
    end

    def sanitized_mobile_regex
      /\A[+][0-9]{11,13}\z/
    end
  end
end
