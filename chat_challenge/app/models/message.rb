# frozen_string_literal: true

class Message < ApplicationRecord
  # Associations
  # --------------

  belongs_to :organization
  belongs_to :conversation
  belongs_to :author, polymorphic: true

  # Validations
  # --------------

  validates :body, presence: true

  # Scopes
  # --------------

  scope :from_officer, -> { where(author_type: LoanOfficer.to_s) }
  scope :last_from_officer, -> { from_officer.last }
end
