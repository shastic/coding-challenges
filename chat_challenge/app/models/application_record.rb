# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  include AuthRegexs

  self.abstract_class = true
end
