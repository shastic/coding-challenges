# frozen_string_literal: true

class LoanOfficer < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  # Associations
  # --------------

  belongs_to :organization
  has_many :messages, as: :author, dependent: :nullify
end
