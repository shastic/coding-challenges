# frozen_string_literal: true

class Conversation < ApplicationRecord
  # Associations
  # ---------------

  belongs_to :organization
  belongs_to :person
  has_many :messages, dependent: :destroy

  # Delegations
  # ---------------

  delegate :name, :mobile, to: :person, prefix: true
  delegate :name, to: :last_contact, prefix: true, allow_nil: true
  delegate :body, to: :last_message, prefix: true, allow_nil: true

  # Methods
  # ---------------

  def last_message
    messages.last
  end

  def last_message_from_officer
    return if messages.blank?

    messages.last_from_officer
  end

  def last_contact
    return if last_message_from_officer.blank?

    last_message_from_officer&.author
  end
end
