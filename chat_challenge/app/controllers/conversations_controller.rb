# frozen_string_literal: true

class ConversationsController < ApplicationController
  def index
    @conversations = conversations.paginate page: params[:page],
                                            per_page: params[:per_page]
  end

  def show; end

  private

  def conversations
    current_organization.conversations
  end
end
