# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :authenticate_loan_officer!

  def current_organization
    @_current_organization = current_loan_officer.organization
  end
end
