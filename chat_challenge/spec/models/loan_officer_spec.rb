# frozen_string_literal: true

require 'rails_helper'

describe LoanOfficer do
  describe 'validations' do
    context 'with valid attributes' do
      subject { create :loan_officer }

      it { is_expected.to be_valid }
    end

    context 'without valid attributes' do
      it { is_expected.to be_invalid }
    end
  end

  describe '#messages' do
    subject(:messages) { loan_officer.messages }

    let(:loan_officer) { create :loan_officer }
    let(:organization) { loan_officer.organization }

    context 'when loan_officer is the author of a message' do
      let!(:message) { create :message, author: loan_officer, organization: organization }

      it 'returns that message' do
        expect(messages).to include message
      end
    end

    context 'when loan_officer is not the author of any messages' do
      it { is_expected.to be_empty }
    end
  end
end
