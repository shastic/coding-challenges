# frozen_string_literal: true

require 'rails_helper'

describe ConversationsController, '#index' do
  context 'when logged in officer' do
    let(:organization) { create :organization }
    let(:loan_officer) { create :loan_officer, organization: organization }

    before { sign_in loan_officer }

    context 'when the organization has conversations' do
      let!(:conversations) { create_list :conversation, 3, organization: organization }

      before { get :index }

      it 'displays conversations' do
        conversations.each do |conversation|
          expect(response.body).to have_selector "#conversation-#{conversation.id}"
        end
      end
    end

    context 'when the organization does not have any conversations' do
      before { get :index }

      it 'displays conversations table' do
        expect(response.body).to have_selector '#conversations'
      end
    end

    context 'when another organization has conversations and the current organization does not' do
      let!(:organization2) { create :organization }
      let!(:conversations) { create_list :conversation, 3, organization: organization2 }

      before { get :index }

      it 'does not display conversations belonging another organization' do
        conversations.each do |conversation|
          expect(response.body).not_to have_selector "#conversation-#{conversation.id}"
        end
      end
    end
  end

  context 'when no officer is logged in' do
    before { get :index }

    it 'redirects to authentication path' do
      expect(response).to redirect_to new_loan_officer_session_path
    end
  end
end
