# frozen_string_literal: true

FactoryBot.define do
  factory :person do
    sequence(:name) { |n| "Dummy User #{n}" }
    sequence(:mobile) { |n| "+1415000#{n.to_s.rjust(4, '0')}" }
  end
end
