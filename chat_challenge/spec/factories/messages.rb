# frozen_string_literal: true

FactoryBot.define do
  factory :message do
    association :organization, factory: :organization
    association :conversation, factory: :conversation
    association :author, factory: :person
    body { 'this is a message' }

    trait :from_person do
      association :author, factory: :person
      body { 'this is a question' }
    end

    trait :from_loan_officer do
      association :author, factory: :loan_officer
      body { 'this is a response' }
    end
  end
end
