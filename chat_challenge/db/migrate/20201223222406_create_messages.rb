class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.text :body
      t.references :organization, foreign_key: true
      t.references :conversation, foreign_key: true
      t.references :author, polymorphic: true

      t.timestamps
    end
  end
end
