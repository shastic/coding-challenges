require "./boot"

data_records_file = "spec/fixtures/records.json"
encoded_records = File.read(data_records_file)
transformed_records = Commands::Ingest.call(records: encoded_records)

puts "Transformed records:"
puts "############################################################"
puts JSON.pretty_generate(transformed_records)
puts "\n############################################################"