ENV['BUNDLE_GEMFILE'] ||= File.expand_path('../Gemfile', __dir__)
APP_ENV = ENV.fetch('APPLICATION_ENV', "development").freeze
APP_ROOT = File.dirname(File.expand_path(__dir__, './extract_transform'))

require 'pry'
require 'dry-types'
require 'dry-struct'
require 'dry-initializer'
require 'yaml'
require 'erb'
require 'uri'
require 'date'

require "active_support"
require "active_support/core_ext"
require "active_support/all"

Dir.glob(File.expand_path('lib/**/*.rb')).sort.each { |f| require f }
Dir.glob(File.expand_path('app/**/*.rb')).sort.each { |f| require f }