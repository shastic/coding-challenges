## Developer Competency Test - Extract+Transform

### Business Context

This test involves a very simple application that parses, filters, and transforms data records. 

Its purpose is to return the records after applying a series of transformations to each of them as well as the collection itself.

For example:

Input record
```json
{
  "invocationId": "1a4072cd-c70c-4ca9-87d0-deb81c50ada5",
  "records": [
    {
      "recordId": "49624726681625080004286593641868598698675428285753589765000000",
      "approximateArrivalTimestamp": 1641337662064,
      "data": "eyJldmVudF9uYW1lIjoidXNlcl9pbmZvIiwiZXZlbnRfdGltZXN0YW1wIjoi\nMjAyMi0wMS0wNCAyMzozMzowMy4yMTdaIiwiZXhlY3V0aW9uX2lkIjoiM2U3\nMzA4MDEtYWIwZS00OGMwLWI3ZTEtM2IzOTQ5Y2JiNWEzIiwiY29udGV4dCI6\neyJvcmdfaWQiOjM0LCJ2ZW5kb3JfaWQiOiJjcm1fdHJhbnNhY3Rpb24iLCJ2\nZW5kb3JfZW5kcG9pbnQiOiJjcm0uZGVtby5jb20iLCJwYWdlX3VybCI6Imh0\ndHBzOi8vY3JtLmRlbW8uY29tL3Nob3J0X2FwcGxpY2F0aW9uLmh0bWwiLCJ0\ncmFuc2FjdGlvbl9pZCI6IjJvOVpTNmIwYjBQVEZaTzdZZjF2QUNneFN2R2Q4\neTVzIiwiYXV0b2xpbmtfdmVuZG9yX2lkIjoiY3JtIiwidXNlcl9uYW1lIjoi\nRE9FLCBKT0UiLCJ1c2VyX21vYmlsZSI6IjQxNTAwMDEyMzQifX0=\n"
    }
  ]
}
```

Applying transformations
```console
$ bundle exec ruby run.rb
```

Transformed record
```json
{
  "event_name": "user_info",
  "event_timestamp": "2022-01-04 23:33:03.217Z",
  "data": {
    "transaction": {
      "vendor_id": "crm",
      "transaction_id": "2o9ZS6b0b0PTFZO7Yf1vACgxSvGd8y5s"
    },
    "user": {
      "full_name": "DOE, JOE",
      "mobile": "4150001234"
    }
  },
  "metadata": {
    "execution_id": "3e730801-ab0e-48c0-b7e1-3b3949cbb5a3",
    "org_id": 34,
  }
}
```

There are a few **important concerns** about the current implementation that need to be corrected:

1. **Duplication of logic**

  * The Parsing, filtering and transforming is fragmented across sub-commands (i.e. `Commands::ProcessUserInfo`, `Commands::ProcessTransactionInfo`) that each apply the same same base-level logic (parsing, filtering, deduplicating, sorting and transforming). 

2. **Bad separation of concerns**

  * There is too much coupling between the classes/mechanisms applying transformations to the data, and the logic of which transformation to apply to each unique type of data record received (i.e. `transaction_info`, `user_info`). If a new type of data object needs to be processed, the team would be forced to modify the codebase. 

  * The business logic related to knowing how to map and transform each type of record to its final transformed schema is completely hardcoded and coupled to individual ruby classes. 

### Your primary objectives

1. **DRY and Re-factorize**

Re-factorize the entire solution to DRY the implementation and to de-couple the ruby logic related to applying a generic data transformation from the business logic related to how to apply such transformation to each unique type of record. The re-factorized solution must accept a JSON configuration directing each type of transformation that needs to be applied to each type of data record. 

For example:

This is only a suggestion for a potential schema of the JSON configuration provided as input to `Commands::Ingest`:

```json
{
  "Transformations": [
    {
      "Type": "ParseJson",
      "Parameters": {
        ...
      }
    },
    ...
  ]
}
```

**It's mission critical to ensure that the re-factorized solution continues to output the same transformed version of the records as specified in the existing (happy path) integration test.**

2. **Write re-factorized tests**

Write re-factorized (clean and modular) RSpec tests for the important edge-cases.

### Acceptance criteria

* All re-factorized code and tests must be DRY. 

* The `Commands::Ingest` command must accept configuration related to how to transform each record type as an argument. 

* Your re-factorized solution should **only** include 1 command class: `Commands::Ingest`. All other commands classes should be eliminated.

* For each unique type of transformation logic (i.e. slicing, filtering, augmenting) there should be **only 1 class** with the single responsibility of applying each type of transformation. These individual transformer classes may accept an input as well as parameters provided by external configuration (if needed) and should returned the transformed record as output. For example, for parsing a string as json, there should only be 1 class that performs that transformation by accepting an input and returning the parsed output. 

* There is an existing integration test for the happy path at `spec/integration_spec.rb`. This test should continue to pass without modifying its specification (`expected_transformed_records`).

 