require "spec_helper"

describe Commands::Ingest do
  
  subject { described_class.call(records: records) }
  
  let(:records) { File.read("spec/fixtures/records.json") }
  let(:transformed_records) { subject }
  let(:expected_transformed_records) do
    [
      hash_including(
        event_name: "input_change",
        event_timestamp: "2021-12-15 16:10:01.075Z",
        data: {
          transaction: {
            element_id: "pre_sb_ls_cboStatus", 
            element_value: "FOO", 
            transaction_id: "Q8akHDJh2e0WMRzoQ6NIg66MqtGZEr7z", 
            vendor_id: "crm"
          }
        },
        metadata: {
          org_id: 34
        }
      ),
      hash_including(
        event_name: "user_info",
        event_timestamp: "2022-01-04 23:33:03.217Z",
        data: {
          user: {
            full_name: "DOE, JOE", 
            mobile: "4150001234"
          }, 
          transaction: {
            transaction_id: "2o9ZS6b0b0PTFZO7Yf1vACgxSvGd8y5s", 
            vendor_id: "crm"
          }
        },
        metadata: {
          execution_id: "3e730801-ab0e-48c0-b7e1-3b3949cbb5a3",
          org_id: 34
        }
      ),
      hash_including(
        event_name: "funding_status", 
        event_timestamp: "2022-01-24 19:59:26.834Z", 
        metadata: {
          org_id: 70
        }, 
        data: {
          transaction: {
            transaction_id: "554f1faf01e14429915f1657c1110ba9", 
            vendor_id: "crm"
          }
        }
      ),
      hash_including(
        event_name: "user_info",
        event_timestamp: "2022-02-08 00:16:21.243Z",
        data: {
          user: {
            full_name: "John Doe", 
            first_name: "John", 
            last_name: "Doe", 
            mobile: "4150001234"
          }, 
          transaction: {
            transaction_id: "554f1faf01e14429915f1657c1110ba9",
            vendor_id: "crm"
          }
        },
        metadata: {
          org_id: 70
        }
      ),
      hash_including(
        event_name: "transaction_on_load",
        event_timestamp: "2022-10-31 14:24:43.606Z",
        data: {
          user: {
            full_name: "DOE JOE",
            mobile: "+14154449625"
          },
          transaction: {
            transaction_id: "2428", 
            transaction_friendly_id: "2428", 
            transaction_uniq_id: "3072242e4fbe4f64b708ae93ecbe669e", 
            vendor_id: "crm", 
            status: "AP", 
            created_at: ["9/12/2022 5:13:18 PM"], 
            transaction_type: "Loan"
          }
        },
        metadata: {
          agent_id: 1427, 
          agent_name: "Harvey Specter", 
          source_id: "34:crm:3072242e4fbe4f64b708ae93ecbe669e:9366eacc4100b2e9b60b20ce123d10ad", 
          org_id: 34, 
          org_name: "Financial Institution One"
        }
      )
    ]
  end
  
  it "extracts and transforms multiple types of records" do
    expect( 
      transformed_records.sort_by { |record| [record[:event_timestamp], record[:event_name]] } 
    ).to contain_exactly(*expected_transformed_records)
  end
  
end