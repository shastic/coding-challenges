require "./boot"
require "rspec"
require "factory_bot"

Dir[File.join(APP_ROOT, "spec", "support", "**", "*.rb")].sort.each { |f| require f }
Dir[File.join(APP_ROOT, "spec", "factories", "**", "*.rb")].sort.each { |f| require f }
Dir[File.join(APP_ROOT, "spec", "shared_examples", "**", "*.rb")].sort.each { |f| require f }

RSpec.configure do |config|
  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
    expectations.syntax = %i[should expect]
  end
  
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end
  
  config.default_formatter = 'documentation'

  config.include FactoryBot::Syntax::Methods
  config.shared_context_metadata_behavior = :apply_to_host_groups

  config.filter_run_when_matching :focus
end
