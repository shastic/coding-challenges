class Array
  def deep_symbolize_keys
    map do |item|
      if item.is_a?(Hash)
        item.deep_symbolize_keys
      elsif item.is_a?(Array)
        item.deep_symbolize_keys
      else
        item
      end
    end
  end
end