# The Base command class.
module Commands; end

module Command
  class Base
    
    class << self
      def call(*args)
        attrs = args.extract_options!
        new(**attrs).call
      end

      # Define the attributes.
      # They are set when initializing the command as keyword arguments and
      # are all accessible as getter methods.
      #
      # ex: `attributes :post, :user, :ability`
      def attributes(*args)
        attr_reader(*args)

        initialize_method_arguments = args.map { |arg| "#{arg}:" }.join(", ")
        initialize_method_body = args.map { |arg| "@#{arg} = #{arg}" }.join(";")

        class_eval <<~CODE, __FILE__, __LINE__ + 1
          def initialize(#{initialize_method_arguments})
            #{initialize_method_body}
          end
        CODE
      end
    end

    def call
      raise NotImplementedError
    end
  end
end

