module Commands
  class Ingest < Command::Base
    EVENT_NAMES = {
      user_info: "user_info",
      input_change: "input_change",
      flow_capture: "flow_capture",
      transaction_on_load: "transaction_on_load",
    }.freeze

    attributes :records

    def call
      return nil if records.blank?
      
      parse_records_payload
      decode_records
      normalize_record_keys
      dedup_records
      split_records_by_type
      execute_subcommands
      
      @transformed_records
    end

    private
    
    # Parse the input JSON payload
    def parse_records_payload
      @parsed_records = JSON.parse(records)                                                             # <- This needs to be controled by JSON configuration
    end
    
    # Base64 decode the `records[].data` attribute of each record and augment with the record's id 
    def decode_records                                                                                  # <- This needs to be controled by JSON
      @decoded_records = @parsed_records["records"].map do |record|
        JSON.parse(                                                                                     
          Base64.decode64(                                                                              
            record["data"]
          )
        ).merge("record_id" => record[:record_id])                                                      
      end
    end
    
    # Convert all records' keys to symbols to standarize access.
    def normalize_record_keys
      @normalized_records = @decoded_records.deep_symbolize_keys                                        # <- This needs to turn into JSON configuration
    end
    
    def dedup_records
      @deduped_records = @normalized_records.uniq { |record| [record[:event_name], record[:event_timestamp]] }
    end
    
    # Split records by type
    def split_records_by_type
      @user_info_records = []
      @transaction_on_load_records = []

      @deduped_records.each do |record|
        case record[:event_name]
        when EVENT_NAMES[:user_info]
          @user_info_records << record
        when EVENT_NAMES[:input_change]
          @transaction_on_load_records << record
        when EVENT_NAMES[:transaction_on_load]
          @transaction_on_load_records << record
        when EVENT_NAMES[:flow_capture]
          if flow_capture_user_info?(record)
            @user_info_records << record
          elsif flow_capture_funding_status?(record)
            @transaction_on_load_records << record
          else
            @transaction_on_load_records << record
          end
        end
      end
    end

    def execute_subcommands
      @transformed_records = []
      
      if @user_info_records.any?
        @transformed_records += Commands::ProcessUserInfo.call(records: @user_info_records)
      end
      
      if @transaction_on_load_records.any?
        @transformed_records += Commands::ProcessTransactionOnLoad.call(records: @transaction_on_load_records) 
      end
    end
    
    def flow_capture_user_info?(record)
      record[:event_name] == "flow_capture" and record[:context][:flow_name] == "user_info"
    end
    
    def flow_capture_funding_status?(record)
      record[:event_name] == "flow_capture" and record[:context][:flow_name] == "funding_status"
    end
  end
end