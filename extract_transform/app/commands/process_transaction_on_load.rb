module Commands
  class ProcessTransactionOnLoad < Command::Base
    
    attributes :records

    def call
      return nil if records.blank?
      map_records
    end

    private

    def map_records
      @mapped_records = TransactionRecordMapper.new(records).call
    end
  end
end