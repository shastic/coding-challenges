class UserRecordMapper
  extend Dry::Initializer
  
  USER_INFO_RECORD_MAPPINGS = {
    event_name: :event_name,
    event_timestamp: :event_timestamp,
    "context.user_name": "data.user.full_name",
    "context.user_mobile": "data.user.mobile",
    "execution_id": "metadata.execution_id",
    "context.org_id": "metadata.org_id"
  }.freeze
  
  FLOW_CAPTURE_USER_INFO_RECORD_MAPPINGS = {
    "context.flow_name": :event_name,
    event_timestamp: :event_timestamp,
    "context.flow_data.user_name": "data.user.full_name",
    "context.flow_data.first_name": "data.user.first_name",
    "context.flow_data.last_name": "data.user.last_name",
    "context.flow_data.cell_phone": "data.user.mobile",
    "context.flow_data.user_mobile": "data.user.mobile",
    "context.org_id": "metadata.org_id"
  }.freeze

  TRANSACTION_DATA_MAPPING = {
    "context.transaction_id": "data.transaction.transaction_id",
    "context.autolink_vendor_id": "data.transaction.vendor_id",
    "context.transaction_status": "data.transaction.status",
    "context.transaction_funding_status": "data.transaction.funding_status"
  }

  param :records, default: -> { [] }, reader: true
  
  def call
    map_records
  end

  private

  def map_records                                                                               # <- This needs to be controled by JSON configuration
    @parsed_records = records.inject([]) do |parsed, record|
      if user_info?(record)
        mappings = USER_INFO_RECORD_MAPPINGS
      elsif flow_capture_user_info?(record)
        mappings = FLOW_CAPTURE_USER_INFO_RECORD_MAPPINGS
      end

      sliced_record = flatten_hash(record).slice(*mappings.keys)
      filtered_record_mapping = filter_mapping_values(sliced_record, mappings)
      filtered_record = sliced_record.transform_keys(&filtered_record_mapping.method(:[]))
      compact_record = filtered_record.reject! { |key, _| key.nil? || key.to_s.strip.empty? }
      transaction_data = extract_transaction_data(record)
      argumented_record = filtered_record.merge(transaction_data)
      argumented_record[:source_id] = generate_source_id(record) if argumented_record[:source_id].blank?
      rehydrated_record = hydrate_flat_keys(argumented_record)
      
      parsed << rehydrated_record
    end
  end

  def extract_transaction_data(record)
    flatten_hash(record).slice(*TRANSACTION_DATA_MAPPING.keys).transform_keys(&TRANSACTION_DATA_MAPPING.method(:[]))
  end

  def extract_user_data(record)
    flatten_hash(record).slice(*USER_DATA_MAPPING.keys).transform_keys(&USER_DATA_MAPPING.method(:[]))
  end
  
  def flow_capture_user_info?(record)
    record[:event_name] == "flow_capture" and record[:context][:flow_name] == "user_info"
  end
  
  def user_info?(record)
    record[:event_name] == "user_info"
  end

  def input_change?(record)
    record[:event_name] == "input_change"
  end

  def generate_source_id(record)
    Digest::MD5.hexdigest([record[:context][:page_url], record[:event_name]].join("-"))
  end

  def flatten_hash(hash)
    hash.each_with_object({}) do |(k, v), hs|
      if v.is_a? Hash
        hs[k.to_sym] = v
        flatten_hash(v).map do |h_k, h_v|
          hs["#{k}.#{h_k}".to_sym] = h_v
        end
      elsif v.is_a? Array
        hs[k.to_sym] = v
        v.each_with_index.map do |a_h, i|
          hs[k.to_sym][i] ||= []
          hs[k.to_sym][i] = a_h
          next unless a_h.is_a? Hash

          flatten_hash(a_h).map do |a_h_k, a_h_v|
            hs["#{k}[#{i}].#{a_h_k}".to_sym] = a_h_v
          end
        end
      else
        hs[k] = v
      end
    end
  end
  
  def filter_mapping_values(record, mapping)
    duplicated_mapping = select_duplicated_keys(mapping)
    keep_these_keys = duplicated_mapping.map do |_k, v|
      v.select { |key| record[key].present? }.last
    end
    all_duplicated_keys = duplicated_mapping.values.flatten
    mapping.except(*(all_duplicated_keys - keep_these_keys))
  end

  def select_duplicated_keys(hash)
    grouped_keys = hash.keys.group_by { |k| hash[k] }
    grouped_keys.select do |_k, v|
      v.length > 1
    end
  end
  
  def hydrate_flat_keys(hash)
    hash.each_with_object({}.with_indifferent_access) do |(k, v), a|
      if k.to_s.split(".").length > 1
        nested_hash = hash_from_nested_key(k, v)
        a.deep_merge!(nested_hash)
      else
        a[k] = v
      end
    end.deep_symbolize_keys
  end

  def hash_from_nested_key(key, value)
    parts = key.to_s.split(".")
    return { key => value } if (parts.length == 1)

    { parts.first => hash_from_nested_key(parts[1..-1].join("."), value) }
  end
end